# passerelle-imio-liege-rn - passerelle connector to Liege NR webservices
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.db import models
from django.utils.translation import gettext_lazy as _
from passerelle.base.models import BaseResource
from passerelle.utils.api import endpoint


class ImioLiegeRn(BaseResource):
    service_url = models.CharField(
        max_length=256, blank=False, verbose_name=_('Service URL'), help_text=_('Gateway Web Service URL')
    )
    verify_cert = models.BooleanField(default=True, verbose_name=_('Check HTTPS Certificate validity'))

    category = _('Misc')

    class Meta:
        verbose_name = _('Liege RN Services')

    @classmethod
    def get_connector_slug(cls):
        return 'imio-liege-rn'

    @endpoint(
        perm='can_access',
        pattern=r'^(?P<nrn>\d+)/$',
        methods=['post', 'get'],
        example_pattern='{nrn}/',
        parameters={
            'nrn': {'description': _('National Register Number'), 'example_value': '79061607381'},
            'caller_name': {'description': _('Caller Name'), 'example_value': 'EGUICHET'},
            'caller_user': {'description': _('Caller User'), 'example_value': 'EGUICHET'},
            'caller_environment': {'description': _('Caller environment'), 'example_value': 'TEST'},
            'caller_version': {'description': _('Caller version'), 'example_value': '2.1.4'},
            'caller_reason': {'description': _('Caller reason'), 'example_value': 'authentification'},
        },
    )
    def data(self, request, nrn, caller_name, caller_user, caller_version, caller_environment, caller_reason):
        str_data = ''
        if request.body is not None and request.body != b'':
            json_acceptable_string = request.body.replace("'", "\"")
            data = json.loads(json_acceptable_string)
            str_data = ''.join([f'&{k}={v}' for k, v in data.items()])
        else:
            str_data = '&caller_name={}&caller_user={}&caller_environment={}&caller_version={}&caller_reason={}'.format(
                caller_name, caller_user, caller_environment, caller_version, caller_reason
            )
        response = self.requests.get(self.service_url + nrn + str_data, verify=self.verify_cert, timeout=5)
        if response.status_code != 200:
            return {}

        attributes = response.json()

        # remove junk from street name
        if attributes.get('rue'):
            attributes['rue'] = attributes['rue'].split('(')[0]
        # use dd/mm/yyyy as date format for birthday
        if attributes.get('dateNaissance'):
            attributes['dateNaissance'] = attributes['dateNaissance'].replace('.', '/')
        # assume all unspecified answers are for Belgium, this may not actually be true
        if not attributes.get('pays'):
            attributes['pays'] = 'Belgique'

        return attributes
