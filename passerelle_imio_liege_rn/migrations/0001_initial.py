from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0005_resourcelog'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImioLiegeRn',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(verbose_name='Identifier', unique=True)),
                ('description', models.TextField(verbose_name='Description')),
                (
                    'log_level',
                    models.CharField(
                        default=b'INFO',
                        max_length=10,
                        verbose_name='Log Level',
                        choices=[
                            (b'NOTSET', b'NOTSET'),
                            (b'DEBUG', b'DEBUG'),
                            (b'INFO', b'INFO'),
                            (b'WARNING', b'WARNING'),
                            (b'ERROR', b'ERROR'),
                            (b'CRITICAL', b'CRITICAL'),
                            (b'FATAL', b'FATAL'),
                        ],
                    ),
                ),
                (
                    'service_url',
                    models.CharField(
                        help_text='Gateway Web Service URL', max_length=128, verbose_name='Service URL'
                    ),
                ),
                (
                    'verify_cert',
                    models.BooleanField(default=True, verbose_name='Check HTTPS Certificate validity'),
                ),
                ('users', models.ManyToManyField(to='base.ApiUser', blank=True)),
            ],
            options={
                'verbose_name': 'Liege RN Services',
            },
        ),
    ]
