from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_imio_liege_rn', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imioliegern',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
